/** @mainpage motion_detect - cargt
 *
 * @author Mark Vogt <mark.vogt@cargtconsulting.com>
 * @version 1.0.0
**/


#include <iostream>
#include <stdio.h>
#include <time.h>
#include <opencv2/opencv.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc_c.h"

using namespace cv;
using namespace std;

inline int detectMotion(const Mat & motion, Mat & result, Mat & result_cropped,
                 int x_start, int x_stop, int y_start, int y_stop,
                 int max_deviation,
                 Scalar & color);

int main(int argc, char** argv )
{
	Mat src;
	VideoWriter writer;

	/*
	 * OPEN CAMERA
	 */
    VideoCapture cap("/dev/video0", cv::CAP_V4L); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    cap.set(CV_CAP_PROP_FRAME_WIDTH,1920);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,1080);
	int width = 1920;//640;
	int height = 1080;//480;

//    cap.set(CV_CAP_PROP_FPS,5);

    // get one frame from camera to know frame size and type
    cap >> src;
    // check if we succeeded
    if (src.empty()) {
        cerr << "ERROR! blank frame grabbed\n";
        return -1;
    }
    bool isColor = (src.type() == CV_8UC3);

    Mat result, result_cropped;
    Mat prev_frame,next_frame, current_frame;
    cap >> prev_frame;
	cap >> next_frame;
	cap >> current_frame;
    cvtColor(current_frame, current_frame, CV_RGB2GRAY);
    cvtColor(prev_frame, prev_frame, CV_RGB2GRAY);
    cvtColor(next_frame, next_frame, CV_RGB2GRAY);


    // d1 and d2 for calculating the differences
    // result, the result of and operation, calculated on d1 and d2
    // number_of_changes, the amount of changes in the result matrix.
    // color, the color for drawing the rectangle when something has changed.
    Mat d1, d2, motion;
    int number_of_changes, number_of_sequence = 0;
    Scalar mean_, color(0,255,255); // yellow

    // Detect motion in window
    int x_start = 10, x_stop = current_frame.cols-11;
    int y_start = 10, y_stop = current_frame.rows-11;

    // If more than 'there_is_motion' pixels are changed, we say there is motion
    // and store an image on disk
    int there_is_motion = 20;

    // Maximum deviation of the image, the higher the value, the more motion is allowed
//    int max_deviation = 20;
    int max_deviation = 200;

    // Erode kernel
    Mat kernel_ero = getStructuringElement(MORPH_RECT, Size(2,2));

    // All settings have been set, now go in endless loop and
    // take as many pictures you want..


	while (1)
	{
		/* edge detection */
//		V4LWrapper_QueryFrame (&mycamera, buffer);
//		V4LWrapper_CvtColor (buffer, image->imageData, width, height, YUV422toRGB888);
//		cvCvtColor (image, gray, CV_RGB2GRAY);
//		cvCanny (gray, gray, 60, 110, 3);
//		cvShowImage ("test", gray);
////		V4LWrapper_OutputDisplay (&mydisplay, buffer);
//
//		cvWaitKey (10);

        // Take a new image
        prev_frame = current_frame;
        current_frame = next_frame;
    	cap >> next_frame;
        result = next_frame;
        cvtColor(next_frame, next_frame, CV_RGB2GRAY);

        // Calc differences between the images and do AND-operation
        // threshold image, low differences are ignored (ex. contrast change due to sunlight)
        absdiff(prev_frame, next_frame, d1);
        absdiff(next_frame, current_frame, d2);
        bitwise_and(d1, d2, motion);
        threshold(motion, motion, 35, 255, CV_THRESH_BINARY);
        erode(motion, motion, kernel_ero);

        number_of_changes = detectMotion(motion, result, result_cropped,  x_start, x_stop, y_start, y_stop, max_deviation, color);
        //imshow ("test", result);
        cout << "Changes: " << number_of_changes << endl;

        // If a lot of changes happened, we assume something changed.
//        if(number_of_changes>=there_is_motion)
//        {
//
//            if(number_of_sequence>0){
//                saveImg(result,DIR,EXT,DIR_FORMAT.c_str(),FILE_FORMAT.c_str());
//                saveImg(result_cropped,DIR,EXT,DIR_FORMAT.c_str(),CROPPED_FILE_FORMAT.c_str());
//            }
//            number_of_sequence++;
//        }
//        else

        if( number_of_changes > there_is_motion )
        {
        	/*
        	 *  INITIALIZE VIDEOWRITER
        	 */

        	if(number_of_sequence == 0)
        	{
				int codec = CV_FOURCC('M', 'J', 'P', 'G');  // select desired codec (must be available at runtime)
				double fps = 5.0;                          // framerate of the created video stream
				time_t t = time(NULL);
				struct tm tm = *localtime(&t);
				char filename_char[200];
				sprintf(filename_char,"/home/root/%04d%02d%02d-%02d%02d%02d.avi", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
				string filename(filename_char);
				writer.open(filename, codec, fps, src.size(), isColor);
        	}

        	// check if we succeeded
        	if (!writer.isOpened()) {
        		return -1;
        	}

			// encode the frame into the videofile stream
			writer.write(result);

        	number_of_sequence++;
        }
        else
        {
        	if(writer.isOpened())
        	{
        		writer.release();
        	}
            number_of_sequence = 0;
            // Delay, wait a 1/2 second.
            cvWaitKey (1000);
        }
	}











    return 0;
}

// Check if there is motion in the result matrix
// count the number of changes and return.
inline int detectMotion(const Mat & motion, Mat & result, Mat & result_cropped,
                 int x_start, int x_stop, int y_start, int y_stop,
                 int max_deviation,
                 Scalar & color)
{
    // calculate the standard deviation
    Scalar mean, stddev;
    meanStdDev(motion, mean, stddev);
    // if not to much changes then the motion is real (neglect agressive snow, temporary sunlight)
    if(stddev[0] < max_deviation)
    {
        int number_of_changes = 0;
        int min_x = motion.cols, max_x = 0;
        int min_y = motion.rows, max_y = 0;
        // loop over image and detect changes
        for(int j = y_start; j < y_stop; j+=2){ // height
            for(int i = x_start; i < x_stop; i+=2){ // width
                // check if at pixel (j,i) intensity is equal to 255
                // this means that the pixel is different in the sequence
                // of images (prev_frame, current_frame, next_frame)
                if(static_cast<int>(motion.at<uchar>(j,i)) == 255)
                {
                    number_of_changes++;
                    if(min_x>i) min_x = i;
                    if(max_x<i) max_x = i;
                    if(min_y>j) min_y = j;
                    if(max_y<j) max_y = j;
                }
            }
        }
        if(number_of_changes){
            //check if not out of bounds
            if(min_x-10 > 0) min_x -= 10;
            if(min_y-10 > 0) min_y -= 10;
            if(max_x+10 < result.cols-1) max_x += 10;
            if(max_y+10 < result.rows-1) max_y += 10;
            // draw rectangle round the changed pixel
            Point x(min_x,min_y);
            Point y(max_x,max_y);
            Rect rect(x,y);
            Mat cropped = result(rect);
            cropped.copyTo(result_cropped);
            rectangle(result,rect,color,1);
        }
        return number_of_changes;
    }
    return 0;
}
